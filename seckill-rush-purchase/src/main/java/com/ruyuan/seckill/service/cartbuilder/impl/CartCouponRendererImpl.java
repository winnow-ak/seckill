package com.ruyuan.seckill.service.cartbuilder.impl;

import com.ruyuan.seckill.domain.Buyer;
import com.ruyuan.seckill.domain.MemberCoupon;
import com.ruyuan.seckill.domain.vo.CartVO;
import com.ruyuan.seckill.domain.vo.CouponVO;
import com.ruyuan.seckill.domain.vo.SelectedPromotionVo;
import com.ruyuan.seckill.service.CartPromotionManager;
import com.ruyuan.seckill.service.cartbuilder.CartCouponRenderer;
import com.ruyuan.seckill.utils.CouponValidateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 购物车优惠券渲染实现
 */
@Service

public class CartCouponRendererImpl implements CartCouponRenderer {


    @Autowired
    private CartPromotionManager cartPromotionManager;

    @Override
    public List<CouponVO> render(List<CartVO> cartList) {
        List<Integer> sellerList = new ArrayList<>();

        //商家id
        cartList.forEach(cartVO -> {
            sellerList.add(cartVO.getSellerId());
        });
        Integer[] sellerIds = new Integer[sellerList.size()];

        //查询出这些店铺的所有优惠券(包括平台)
        // List<MemberCoupon> couponList = this.memberCouponClient.listByCheckout(sellerList.toArray(sellerIds), UserContext.getBuyer().getUid());
        List<MemberCoupon> couponList = new ArrayList<>();

        //填充购物车的优惠券列表
        return fillOneCartCoupon(cartList, couponList);

    }


    @Override
    public List<CouponVO> render(List<CartVO> cartList, Buyer buyer) {
        List<Integer> sellerList = new ArrayList<>();

        //商家id
        cartList.forEach(cartVO -> {
            sellerList.add(cartVO.getSellerId());
        });
        Integer[] sellerIds = new Integer[sellerList.size()];

        //查询出这些店铺的所有优惠券(包括平台)
        // List<MemberCoupon> couponList = this.memberCouponClient.listByCheckout(sellerList.toArray(sellerIds), UserContext.getBuyer().getUid());
        List<MemberCoupon> couponList = new ArrayList<>();

        //填充购物车的优惠券列表
        return fillOneCartCoupon(cartList,buyer, couponList);

    }

    /**
     * 填充一个购物车的优惠劵
     *
     * @param cartList
     * @param couponList
     * @return
     */
    private List<CouponVO> fillOneCartCoupon(List<CartVO> cartList, List<MemberCoupon> couponList) {


        //如果购物车中包含积分商品，则无需渲染积分商品不能使用优惠券  add by liuyulei 2019-05-14
        SelectedPromotionVo selectedPromotionVo = cartPromotionManager.getSelectedPromotion();
        boolean isEnable = false;
        CouponVO selectedSiteCoupon = selectedPromotionVo.getCouponMap().get(0);
        Map<Integer, CouponVO> selectedCouponMap = new HashMap<>();
        for (CartVO cartVO : cartList) {
            isEnable = CouponValidateUtil.validateCoupon(selectedPromotionVo, cartVO.getSellerId(), cartVO.getSkuList());
            if (isEnable) {
                //存在积分商品
                break;
            }
            //查找可能已经选中的的优惠劵
            if (selectedSiteCoupon != null) {
                //选中了站点优惠券
                selectedCouponMap.put(0, selectedSiteCoupon);
            } else {
                CouponVO selectedCoupon = selectedPromotionVo.getCouponMap().get(cartVO.getSellerId());
                if (selectedPromotionVo.getCouponMap().get(cartVO.getSellerId()) != null) {
                    selectedCouponMap.put(cartVO.getSellerId(), selectedCoupon);
                }
            }
        }

        //要形成的购物车优惠券列表
        List<CouponVO> cartCouponList = new ArrayList<>();
        //查看优惠券的范围
        // for (MemberCoupon coupon : couponList) {
        //
        //     CouponVO couponVO = new CouponVO(coupon);
        //     //判读是否存在积分商品，如果存在则不能使用优惠券
        //     if (isEnable) {
        //         couponVO.setEnable(0);
        //         couponVO.setErrorMsg("当前购物车内包含积分商品，不能使用优惠券！");
        //         couponVO.setSelected(0);
        //         cartCouponList.add(couponVO);
        //         continue;
        //     }
        //     //查看该优惠券是否可用
        //     CouponValidateResult enableRes = CouponValidateUtil.isEnable(coupon, cartList);
        //     if (enableRes.isEnable()) {
        //         //该优惠券可用
        //         couponVO.setEnable(1);
        //         //设置可以使用该优惠券的商品
        //         couponVO.setEnableSkuList(enableRes.getSkuIdList());
        //         //如果购物车存在优惠劵  当优惠券可用时才设置是否选中
        //         CouponVO selectedCoupon = selectedCouponMap.get(coupon.getSellerId());
        //         if (selectedCoupon != null && selectedCoupon.getMemberCouponId().intValue() == couponVO.getMemberCouponId().intValue()) {
        //             couponVO.setSelected(1);
        //         } else {
        //             couponVO.setSelected(0);
        //         }
        //     } else {
        //         couponVO.setEnable(0);
        //         couponVO.setErrorMsg("订单金额不满足此优惠券使用金额！");
        //     }
        //     cartCouponList.add(couponVO);
        // }

        return cartCouponList;
    }

    /**
     * 填充一个购物车的优惠劵
     *
     * @param cartList
     * @param couponList
     * @return
     */
    private List<CouponVO> fillOneCartCoupon(List<CartVO> cartList, Buyer buyer,List<MemberCoupon> couponList) {
        //如果购物车中包含积分商品，则无需渲染积分商品不能使用优惠券
        SelectedPromotionVo selectedPromotionVo = cartPromotionManager.getSelectedPromotion(buyer);
        boolean isEnable = false;
        CouponVO selectedSiteCoupon = selectedPromotionVo.getCouponMap().get(0);
        Map<Integer, CouponVO> selectedCouponMap = new HashMap<>();
        for (CartVO cartVO : cartList) {
            isEnable = CouponValidateUtil.validateCoupon(selectedPromotionVo, cartVO.getSellerId(), cartVO.getSkuList());
            if (isEnable) {
                //存在积分商品
                break;
            }
            //查找可能已经选中的的优惠劵
            if (selectedSiteCoupon != null) {
                //选中了站点优惠券
                selectedCouponMap.put(0, selectedSiteCoupon);
            } else {
                CouponVO selectedCoupon = selectedPromotionVo.getCouponMap().get(cartVO.getSellerId());
                if (selectedPromotionVo.getCouponMap().get(cartVO.getSellerId()) != null) {
                    selectedCouponMap.put(cartVO.getSellerId(), selectedCoupon);
                }
            }
        }

        //要形成的购物车优惠券列表
        List<CouponVO> cartCouponList = new ArrayList<>();
        //查看优惠券的范围
        // for (MemberCoupon coupon : couponList) {
        //
        //     CouponVO couponVO = new CouponVO(coupon);
        //     //判读是否存在积分商品，如果存在则不能使用优惠券
        //     if (isEnable) {
        //         couponVO.setEnable(0);
        //         couponVO.setErrorMsg("当前购物车内包含积分商品，不能使用优惠券！");
        //         couponVO.setSelected(0);
        //         cartCouponList.add(couponVO);
        //         continue;
        //     }
        //     //查看该优惠券是否可用
        //     CouponValidateResult enableRes = CouponValidateUtil.isEnable(coupon, cartList);
        //     if (enableRes.isEnable()) {
        //         //该优惠券可用
        //         couponVO.setEnable(1);
        //         //设置可以使用该优惠券的商品
        //         couponVO.setEnableSkuList(enableRes.getSkuIdList());
        //         //如果购物车存在优惠劵  当优惠券可用时才设置是否选中
        //         CouponVO selectedCoupon = selectedCouponMap.get(coupon.getSellerId());
        //         if (selectedCoupon != null && selectedCoupon.getMemberCouponId().intValue() == couponVO.getMemberCouponId().intValue()) {
        //             couponVO.setSelected(1);
        //         } else {
        //             couponVO.setSelected(0);
        //         }
        //     } else {
        //         couponVO.setEnable(0);
        //         couponVO.setErrorMsg("订单金额不满足此优惠券使用金额！");
        //     }
        //     cartCouponList.add(couponVO);
        // }

        return cartCouponList;
    }

}
