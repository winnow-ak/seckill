package com.ruyuan.seckill.service.cartbuilder;


import com.ruyuan.seckill.domain.Buyer;
import com.ruyuan.seckill.domain.vo.CartVO;

import java.util.List;

/**
 * 购物车促销信息渲染器
 */
public interface CartPromotionRenderer {


    /**
     * 对购物车进行渲染促销数据
     * @param cartList
     * @return
     */
    void render(List<CartVO> cartList);

    void render(List<CartVO> cartList, Buyer buyer);

}
