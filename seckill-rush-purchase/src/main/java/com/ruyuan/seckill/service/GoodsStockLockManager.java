package com.ruyuan.seckill.service;

import com.ruyuan.seckill.domain.vo.TradeVO;

/**
  * 订单库存管理组件
  */
public interface GoodsStockLockManager {


    /**
     * 秒杀扣除redis库存
     *
     * @param tradeVO 交易信息
     */
    boolean onSeckillRedisStockDeduction(TradeVO tradeVO);


}
