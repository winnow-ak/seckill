package com.ruyuan.seckill.domain.vo;

import java.io.Serializable;
import java.util.List;

/**
 * 购物车原始数据
 */
public class CartSkuOriginVo extends GoodsSkuVO implements Serializable {

    private static final long serialVersionUID = -7457589664804806186L;

    private int num;

    private int checked;

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getChecked() {
        return checked;
    }

    public void setChecked(int checked) {
        this.checked = checked;
    }


    /**
     * 可以参与的单品活动
     */
    private List<CartPromotionVo> singleList;


    /**
     * 可以参与的组合活动
     */
    private List<CartPromotionVo> groupList;

    public List<CartPromotionVo> getSingleList() {
        return singleList;
    }

    public void setSingleList(List<CartPromotionVo> singleList) {
        this.singleList = singleList;
    }

    public List<CartPromotionVo> getGroupList() {
        return groupList;
    }

    public void setGroupList(List<CartPromotionVo> groupList) {
        this.groupList = groupList;
    }

    @Override
    public String toString() {
        return "CartSkuOriginVo{" +
                "num=" + num +
                ", checked=" + checked +
                ", singleList=" + singleList +
                ", groupList=" + groupList +
                '}';
    }

}
